This is qgit.exe statically linked to Trolltech Qt4.3.3 for Windows Open source edition

PREREQUISITES

- You need msysgit (http://code.google.com/p/msysgit/) correctly installed


NOTES

- This version has NOT been tested with cygwin version of git

- In case qgit has problem to find correct msysgit files you should
  add mysysgit\bin directory to your PATH before to start qgit.
  But normally installer should had already take care of this for you.


Please report any problem to the Git Mailing List <git@vger.kernel.org>
specifying [QGit] in your mail subject
