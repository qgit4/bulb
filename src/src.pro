# Under Windows uncomment following line to enable console messages
#CONFIG += ENABLE_CONSOLE_MSG

include(../config.pri)

# General stuff
TEMPLATE = lib
CONFIG += qt warn_on exceptions debug_and_release staticlib
INCLUDEPATH += ../src
MAKEFILE = qmake
TARGET = qgitlib

ENABLE_CONSOLE_MSG {
	CONFIG -= windows
	CONFIG += console
}

# Directories
CONFIG(debug, debug|release) {
    BUILD_DIR = ../build/debug
} else {
    BUILD_DIR = ../build/release
}
DESTDIR = $$BUILD_DIR
UI_DIR = $$BUILD_DIR
MOC_DIR = $$BUILD_DIR
RCC_DIR = $$BUILD_DIR
OBJECTS_DIR = $$BUILD_DIR

# Project files
FORMS += commit.ui console.ui customaction.ui fileview.ui help.ui \
         mainview.ui patchview.ui rangeselect.ui revsview.ui settings.ui

HEADERS += annotate.h cache.h commitimpl.h common.h config.h consoleimpl.h \
           customactionimpl.h dataloader.h domain.h exceptionmanager.h \
           filecontent.h filelist.h fileview.h git.h help.h lanes.h \
           listview.h mainimpl.h myprocess.h patchcontent.h patchview.h \
           rangeselectimpl.h revdesc.h revsview.h settingsimpl.h \
           smartbrowse.h treeview.h

SOURCES += annotate.cpp cache.cpp commitimpl.cpp consoleimpl.cpp \
           customactionimpl.cpp dataloader.cpp domain.cpp exceptionmanager.cpp \
           filecontent.cpp filelist.cpp fileview.cpp git.cpp git_startup.cpp \
           lanes.cpp listview.cpp mainimpl.cpp myprocess.cpp namespace_def.cpp \
	   patchcontent.cpp patchview.cpp rangeselectimpl.cpp revdesc.cpp \
	   revsview.cpp settingsimpl.cpp smartbrowse.cpp treeview.cpp

RESOURCES += icons.qrc

DISTFILES += app_icon.rc helpgen.sh resources/* Src.vcproj todo.txt
DISTFILES += ../COPYING ../exception_manager.txt ../README ../README_WIN.txt
DISTFILES += ../qgit_inno_setup.iss ../QGit4.sln
