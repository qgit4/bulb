# Project files (except qgit.cpp, which is added in src.pro)
FORMS += commit.ui console.ui customaction.ui fileview.ui help.ui \
         mainview.ui patchview.ui rangeselect.ui revsview.ui settings.ui

HEADERS += annotate.h cache.h commitimpl.h common.h config.h consoleimpl.h \
           customactionimpl.h dataloader.h domain.h exceptionmanager.h \
           filecontent.h filelist.h fileview.h git.h help.h lanes.h \
           listview.h mainimpl.h myprocess.h patchcontent.h patchview.h \
           rangeselectimpl.h revdesc.h revsview.h settingsimpl.h \
           smartbrowse.h treeview.h

SOURCES += annotate.cpp cache.cpp commitimpl.cpp consoleimpl.cpp \
           customactionimpl.cpp dataloader.cpp domain.cpp exceptionmanager.cpp \
           filecontent.cpp filelist.cpp fileview.cpp git.cpp git_startup.cpp \
           lanes.cpp listview.cpp mainimpl.cpp myprocess.cpp namespace_def.cpp \
	   patchcontent.cpp patchview.cpp rangeselectimpl.cpp revdesc.cpp \
	   revsview.cpp settingsimpl.cpp smartbrowse.cpp treeview.cpp

RESOURCES += icons.qrc
