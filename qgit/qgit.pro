# Under Windows launch script start_qgit.bat needs the
# value GIT_EXEC_DIR to be set to the git bin directory
GIT_EXEC_DIR = "C:\Program Files\Git\bin"

include(../config.pri)

# General stuff
TEMPLATE = app
CONFIG += qt warn_on exceptions debug_and_release
INCLUDEPATH += ../src
MAKEFILE = qmake

# Platform dependent stuff
win32 {
    TARGET = qgit
    target.path = $$GIT_EXEC_DIR
    CONFIG += windows embed_manifest_exe
    RC_FILE = app_icon.rc
}

unix {
    TARGET = qgit
    target.path = ~/bin
    CONFIG += x11
}

INSTALLS += target

# Directories
CONFIG(debug, debug|release) {
    DESTDIR = ../bin/debug
    BUILD_DIR = ../build/debug
} else {
    DESTDIR = ../bin/release
    BUILD_DIR = ../build/release
}
UI_DIR = $$BUILD_DIR
MOC_DIR = $$BUILD_DIR
RCC_DIR = $$BUILD_DIR
OBJECTS_DIR = $$BUILD_DIR

SOURCES += qgit.cpp
unix:LIBS += $$BUILD_DIR/libqgitlib.a
win32:LIBS += $$BUILD_DIR/qgitlib.lib

# Here we generate a batch called start_qgit.bat used, under Windows only,
# to start qgit with proper PATH set.
#
# NOTE: qgit must be installed in git directory, among git exe files
# for this to work. If you install with 'make install' this is already
# done for you.
#
# Remember to set proper GIT_EXEC_DIR value at the beginning of this file
#
win32 {
    !exists($${GIT_EXEC_DIR}/git.exe) {
        error("I cannot found git files, please set GIT_EXEC_DIR in 'src.pro' file")
    }
    QGIT_BAT = ../start_qgit.bat
    CUR_PATH = $$system(echo %PATH%)
    LINE_1 = $$quote(set PATH=$$CUR_PATH;$$GIT_EXEC_DIR;)
    LINE_2 = $$quote(set PATH=$$CUR_PATH;)

    qgit_launcher.commands =    @echo @echo OFF > $$QGIT_BAT
    qgit_launcher.commands += && @echo $$LINE_1 >> $$QGIT_BAT
    qgit_launcher.commands += && @echo bin\\$$TARGET >> $$QGIT_BAT
    qgit_launcher.commands += && @echo $$LINE_2 >> $$QGIT_BAT

    QMAKE_EXTRA_TARGETS += qgit_launcher
    PRE_TARGETDEPS += qgit_launcher
}
