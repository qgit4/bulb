# check for Qt >= 4.3.0
CUR_QT = $$[QT_VERSION]

# WARNING greaterThan is an undocumented function
!greaterThan(CUR_QT, 4.3) {
	error("Sorry I need Qt 4.3.0 or later, you seem to have Qt $$CUR_QT instead")
}

# check for g++ compiler
contains(QMAKE_CC,.*g\+\+.*) {
	CONFIG += HAVE_GCC
}
contains(QMAKE_CC,.*gcc.*) {
	CONFIG += HAVE_GCC
}

HAVE_GCC {
	QMAKE_CXXFLAGS_RELEASE += -s -O2 -Wno-non-virtual-dtor -Wno-long-long -pedantic
	QMAKE_CXXFLAGS_DEBUG += -g3 -ggdb -O0 -Wno-non-virtual-dtor -Wno-long-long -pedantic
}
