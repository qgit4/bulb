include(../config.pri)

# General stuff
TEMPLATE = app
CONFIG += qt qtestlib warn_on exceptions debug_and_release
INCLUDEPATH += ../src
MAKEFILE = qmake

# Platform dependent stuff
win32 {
    TARGET = test
    CONFIG += windows embed_manifest_exe
}

unix {
    TARGET = test
    CONFIG += x11
}

CONFIG(debug, debug|release) {
    DESTDIR = ../bin/debug
    BUILD_DIR = ../build/debug
} else {
    DESTDIR = ../bin/release
    BUILD_DIR = ../build/release
}
UI_DIR = $$BUILD_DIR
MOC_DIR = $$BUILD_DIR
RCC_DIR = $$BUILD_DIR
OBJECTS_DIR = $$BUILD_DIR

unix:LIBS += $$BUILD_DIR/libqgitlib.a
win32:LIBS += $$BUILD_DIR/qgitlib.lib

# Project files
HEADERS += tests.h
SOURCES += tests.cpp

# Finally, the list of tests:
SOURCES += gittests.cpp
SOURCES += metatests.cpp
