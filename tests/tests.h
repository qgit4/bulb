/*
 * Copyright (C) 2008 Jan Hudec <bulb@ucw.cz>
 *
 * See COPYING file that comes with this distribution
 */
#ifndef TESTS_TESTS_H
#define TESTS_TESTS_H

#include <QDir>
#include <QObject>
#include <QTest>

class TestSuite : public QObject {
Q_OBJECT

	static TestSuite *head;
	TestSuite *next, **prev;

protected:
	QObject *ctxt;
	bool passed;

public:
	static int execInstances(int argc, char **argv);
	TestSuite();
	~TestSuite();

protected slots:
	void init();
	void cleanup();
};

class DirTestSuite : public TestSuite {
Q_OBJECT

	static int count;
	int scriptNo;

protected:
	QDir tempDir;

	bool shellScript(const char *text, qint64 count = -1);

	QByteArray fileContent(const QString &name);

protected slots:
	void init();
	void cleanup();
};

#endif //TESTS_TESTS_H
