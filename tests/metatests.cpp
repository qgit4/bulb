/*
 * Copyright (C) 2008 Jan Hudec <bulb@ucw.cz>
 *
 * See COPYING file that comes with this distribution
 */

#include <QtDebug>

#include <git.h>
#include "tests.h"

class MetaTests : public DirTestSuite {
Q_OBJECT

private slots:
	void testTempDir() {
		QVERIFY(tempDir.exists());
		QVERIFY(shellScript("echo foobar > testfile"));
		QCOMPARE(fileContent("testfile").data(), "foobar\n");
		QCOMPARE(fileContent("00.out").data(), "+ echo foobar\n");
		QVERIFY(!shellScript("no-such-command"));
		QCOMPARE(fileContent("01.out").data(),
			 "+ no-such-command\n"
			 "bash: line 1: no-such-command: command not found\n");
		passed = true;
	}
};

static MetaTests metaTests;

#include "metatests.moc"
