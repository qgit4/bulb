/*
 * Copyright (C) 2008 Jan Hudec <bulb@ucw.cz>
 *
 * See COPYING file that comes with this distribution
 */

#include <QtDebug>

#include <common.h>
#include <git.h>

#include "tests.h"

// FIXME: We should call it qualified or it shouldn't be in a namespace in
// the first place.
using namespace QGit;

class GitTests : public DirTestSuite {
Q_OBJECT
	bool origRangeSelect;

private slots:
	void initTestCase() {
		// Note: we use our own settings file, so it should be OK to
		// tweak stuff like this.
		setFlag(RANGE_SELECT_F, false);
	}

	void openInvalid() {
		Git *repo = new Git(ctxt);
		QVERIFY(repo);
		passed = true;
	}

	void repositoryDetection() {
		bool quit;

		Git *repo = new Git(ctxt);
		QVERIFY(repo);

		// This is normally initialized from Domain. Begs for
		// refactoring -- classes should care for their own
		// invariants
		repo->setDefaultModel(new FileHistory(ctxt, repo));

		repo->stop(true);
		QVERIFY(!repo->init(tempDir.path(), true, NULL, false, &quit));
		repo->stop(true);

		shellScript("git init\n"
			    "echo content of Foo > foo\n"
			    "git add .\n"
			    "git commit -m 'Initial commit'\n");

		QVERIFY(repo->init(tempDir.path(), true, NULL, false, &quit));
		QVERIFY(!repo->isStGITStack());
		repo->stop(true);

		shellScript("git checkout -b st\n"
			    "stg init\n");

		QVERIFY(repo->init(tempDir.path(), true, NULL, false, &quit));
		QVERIFY(repo->isStGITStack());
		repo->stop(true);

		shellScript("git checkout master\n");

		QVERIFY(repo->init(tempDir.path(), true, NULL, false, &quit));
		QVERIFY(!repo->isStGITStack());
		repo->stop(true);

		passed = true;
	}
};

static GitTests gitTests;

#include "gittests.moc"
