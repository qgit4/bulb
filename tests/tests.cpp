/*
 * Copyright (C) 2008 Jan Hudec <bulb@ucw.cz>
 *
 * See COPYING file that comes with this distribution
 */

#include <QApplication>
#include <QDirIterator>
#include <QProcess>
#include <QSettings>
#include <QTest>
#include <QtDebug>

#include "tests.h"
#include "../src/common.h"

using namespace QGit;

/* TestSuite ************************************************************/

TestSuite *TestSuite::head = NULL;

TestSuite::TestSuite() :
	next(head),
	prev(&head),
	ctxt(NULL),
	passed(false) {

	if(next)
	    next->prev = &next;
	*prev = this;
}

TestSuite::~TestSuite() {

	if(next)
	    next->prev = prev;
	*prev = next;
}

int TestSuite::execInstances(int argc, char **argv) {

	TestSuite *instance;
	int res = 0;
	// FIXME: Parse command-line arguments, prepare new argc and argv and
	// only call instances matching the test suites specified (if
	// specified).
	for(instance = head; instance; instance = instance->next)
		res += QTest::qExec(instance, argc, argv);
	return res;
}

void TestSuite::init() {

	ctxt = new QObject(NULL);
	passed = false;
}

void TestSuite::cleanup() {

	delete ctxt;
	ctxt = NULL;
}

static bool rmrf(QDir path) {

	// Safety net: only remove directories in /tmp
	if(!path.path().startsWith(QDir::temp().path()))
	   return false;

	QDirIterator i(path.path(),
		       QDir::AllEntries|QDir::NoDotAndDotDot|QDir::System|QDir::Hidden);
	while(!i.next().isEmpty()) {
		QString n = i.fileName();
		if(i.fileInfo().isDir()) {
			if(!rmrf(QDir(i.filePath())))
				return false;
		} else {
			if(!path.remove(i.filePath()))
				return false;
			//qDebug() << "rm" << i.filePath();
		}
	}
	return path.rmdir(path.path());
	//qDebug() << "rmdir" << path.path();
	//return true;
}

static class TempDirBase {

	bool initialized;
	QDir dir_;

public:
	TempDirBase() : initialized(false) {
	}

	~TempDirBase() {
		if(initialized)
			dir_.rmpath(dir_.path());
	}

	const QDir &dir() {
		if(!initialized) {
			dir_.setPath(QDir::temp().filePath("qgit-test-%1")
				.arg(QCoreApplication::applicationPid()));
			if(dir_.exists())
				rmrf(dir_);
			if(!dir_.mkpath(dir_.path()))
				QTest::qFail(QString("Failed to create directory \"%1\"")
					     .arg(dir_.path()).toAscii(),
					     __FILE__, __LINE__);
			initialized = true;
		}
		return dir_;
	}

} tempDirBase;

int DirTestSuite::count = 0;

void DirTestSuite::init() {

	TestSuite::init();

	tempDir.setPath(tempDirBase.dir().filePath("%1-%2")
			.arg(count++, 5, 10, QChar('0'))
			.arg(metaObject()->className()));
	//qDebug() << "tempDir =" << tempDir;
	if(!tempDir.mkpath(tempDir.path()))
		QFAIL(QString("Failed to create directory \"%1\"")
				.arg(tempDir.path()).toAscii());
	tempDir.setCurrent(tempDir.path());
}

void DirTestSuite::cleanup() {

	TestSuite::cleanup();

	tempDir.setCurrent(tempDirBase.dir().path());

	if(passed) {
		if(!rmrf(tempDir.path()))
			QWARN(QString("Failed to remove directory \"%1\"")
				.arg(tempDir.path()).toAscii());
	} else
		qWarning() << "Leaving temp directory of a failed test"
			<< tempDir.path();
}

bool DirTestSuite::shellScript(const char *text, qint64 count) {

	if(count < 0)
		count = strlen(text);

	QProcess shell;
	shell.setProcessChannelMode(QProcess::MergedChannels);
	shell.setStandardOutputFile(QString("%1.out")
				     .arg(scriptNo++, 2, 10, QChar('0')));
	shell.start(QString("bash"), QStringList() << "-e" << "-x" << "-");
	// NOTE: QVERIFY2 does return, but we need a return value.
#define return return false
	QVERIFY2(shell.waitForStarted(5000), "Failed to start shell.");
	QVERIFY2(count == shell.write(text, count), "Failed to write all data.");
	shell.closeWriteChannel();
	QVERIFY2(shell.waitForFinished(30000), "The script didn't finish in time.");
#undef return
	return shell.exitCode() == 0;
}

QByteArray DirTestSuite::fileContent(const QString &name) {

	QFile f(name);
#define return return NULL
	QVERIFY2(f.open(QIODevice::ReadOnly), "Failed to read file");
#undef return
	QByteArray d = f.readAll();
	f.close();
	return d;
}

/* main *****************************************************************/

int main(int argc, char* argv[]) {

	QApplication app(argc, argv);
	QCoreApplication::setOrganizationName(ORG_KEY);
	QCoreApplication::setApplicationName(APP_KEY);

	/* Use separate .ini file for configuration while testing */
	QSettings::setDefaultFormat(QSettings::IniFormat);
	QSettings::setPath(QSettings::IniFormat, QSettings::SystemScope,
			   tempDirBase.dir().filePath("etc"));
	QSettings::setPath(QSettings::IniFormat, QSettings::UserScope,
			   tempDirBase.dir().filePath("home"));

	/* On Windows msysgit exec directory is set up
	 * during installation so to always find git.exe
	 * also if not in PATH
	 */
	QSettings set;
	GIT_DIR = set.value(GIT_DIR_KEY).toString();

	initMimePix();

	int ret = TestSuite::execInstances(argc, argv);

	freeMimePix();
	return ret;
}

